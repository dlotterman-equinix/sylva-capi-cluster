
# Sylva-capi-cluster

A Helm chart for deploying ClusterAPI and ClusterAPI providers resources

## Configuration

The following table lists the configurable parameters of the Sylva-capi-cluster chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `name` | name of the CAPI cluster | `"workload-cluster"` |
| `air_gapped` | can be set to true to do an RKE2 deployment disconnected from the Internet | `false` |
| `cis_profile` | CIS profile to be used. Curently supported only for rke2 clusters. "cis-1.6" for k8s prior to 1.25, "cis-1.23" for 1.25+ | `""` |
| `default_cni` |  | `"calico"` |
| `k8s_version` | kubernetes version to be used; add "+rke2r1" for cabpr (RKE2) clusters | `"v1.24.12"` |
| `capi_providers.infra_provider` | capd, capo, capm3 or capv | mandatory |
| `capi_providers.bootstrap_provider` | cabpr (RKE2) or cabpk (kubeadm) | mandatory |
| `control_plane_replicas` | nodes number for control plane | `3` |
| `cluster_services_cidrs` | network ranges (in CIDR notation) from which Services networks are allocated | `["10.43.0.0/16"]` |
| `cluster_pods_cidrs` | network ranges (in CIDR notation) from which Pod networks are allocated | `["10.42.0.0/16"]` |
| `dns_resolver` | boolean conditioning DNS client configuration | `false` |
| `enable_longhorn` |  | `false` |
| `cluster_external_ip` | address for kube-api (and other services) exposure | `"10.122.22.151"` |
| `cluster_public_ip` | (optional) when set, the cluster apiServerFixedIP will be set to this address and CAPI will connect to the cluster via this address (typically used with CAPO when a Floating IP is bound to `cluster_external_ip`) | `""` |
| `cluster_external_ip_interfaces` | list, will default to [control_plane.capm3.public_pool_interface] | `[]` |
| `cluster_api_cert_extra_SANs` | (optional) list of extra subjectAltNames to be added to cluster api endpoint certificate, useful if you want to access the api through a NAT or reverse-proxy | `[]` |
| `mgmt_cluster_external_ip` | management cluster external IP used as DNS resolver for Rancher (hosted in a CAPI management cluster in Sylva) FQDN in case `dns_resolver` is set to true | `"11.11.11.11"` |
| `mgmt_cluster_external_domain` | management cluster external domain, for defining the DNS client | `"sylva"` |
| `metallb_helm_oci_url` | OCI registry endpoint for MetalLB chart used in RKE2 deployments; leave it unset/empty for non-OCI based deployments (with Internet access) | `"oci://replace_me"` |
| `metallb_helm_version` |  | `"0.13.9"` |
| 'metallb_helm_extra_ca_certs' | Additional CA certs to be trusted for pulling metallb helm artifacts (one string in PEM-format, with one or more certificates) | "" |
| `capd.docker_host_socket` | host path extra mount for CAPD controller | `"/var/run/docker.sock"` |
| `capd.image_name` | image used for CABPR (RKE2) CAPD CP & MD nodes | `"registry.gitlab.com/sylva-projects/sylva-elements/container-images/rke2-in-docker:v1-24-12-rke2r1"` |
| `capo.image_name` | image used for CAPO CP & MD nodes | `"replace_me"` |
| `capo.flavor_name` |  | `"m1.large"` |
| `capo.rootVolume` | Let this parameter empty if you don't intend to use root volume for CAPO CP & MD nodes | `{}` |
| `capo.control_plane_az` | list of OpenStack availability zones to deploy control planes nodes to, otherwise all would be candidates | `["replace_me"]` |
| `capo.ssh_key_name` | OpenStack VM SSH key | `"replace_me"` |
| `capo.network_id` | OpenStack network used for nodes and VIP port | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.auth_url` | $OS_AUTH_URL | `"https://replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.user_domain_name` | $OS_USER_DOMAIN_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.project_domain_name` | $OS_PROJECT_DOMAIN_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.project_name` | $OS_PROJECT_DOMAIN_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.username` | $OS_USERNAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.password` | $OS_PASSWORD | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.region_name` | $OS_REGION_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.verify` | boolean for server cert check | `false` |
| `capo.cacert` | cert used to validate CA of OpenStack APIs | `"sylva-tag"` |
| `capo.resources_tag` | tag set for OpenStack resources | `"sylva-tag"` |
| `capo.extra.allowed_address_pairs` | list of allowed address pairs config configured on control plane node interface | `[]` |
| `capv.username` | vSphere username | `"replace_me"` |
| `capv.password` | vSphere password | `"replace_me"` |
| `capv.dataCenter` | Datacenter to use | `"*"` |
| `capv.image_name` | image used for CAPV CP & MD nodes | `"replace_me"` |
| `capv.diskGiB` | disk size for CP & MD vSphereMachineTemplate | `25` |
| `capv.memoryMiB` | memory size for CP & MD vSphereMachineTemplate | `8192` |
| `capv.numCPUs` | CPUs for CP & MD vSphereMachineTemplate | `2` |
| `capv.networks.default.networkName` | vSphere network name for VMs and CSI | `"replace_me"` |
| `capv.networks.default.dhcp4` | vSphere network DHCPv4 enable boolean | `true` |
| `capv.server` | vSphere server DNS name or IP | `"replace_me"` |
| `capv.dataStore` | vSphere datastore name | `"replace_me"` |
| `capv.tlsThumbprint` | vSphere https TLS thumbprint | `"replace_me"` |
| `capv.ssh_key` | SSH public key for VM access | `"replace_me"` |
| `capv.folder` | vSphere folder | `"replace_me"` |
| `capv.resourcePool` | vSphere resourcepool | `"replace_me"` |
| `capv.storagePolicyName` | vSphere storage policy name | `"replace_me"` |
| `capv.template_clone_mode` | possible values: "fullClone", "linkedClone" | `"fullClone"` |
| `capm3.public_pool_name` |  | `"public-pool"` |
| `capm3.public_pool_network` |  | `"172.20.36.128"` |
| `capm3.public_pool_gateway` |  | `"172.20.36.129"` |
| `capm3.public_pool_end` |  | `"172.20.36.150"` |
| `capm3.public_pool_start` |  | `"172.20.36.140"` |
| `capm3.public_pool_prefix` |  | `"26"` |
| `capm3.public_pool_interface` |  | `""` |
| `capm3.provisioning_pool_name` |  | `"provisioning-pool"` |
| `capm3.provisioning_pool_network` |  | `"172.20.39.192"` |
| `capm3.provisioning_pool_gateway` |  | `"172.20.39.193"` |
| `capm3.provisioning_pool_end` |  | `"172.20.39.219"` |
| `capm3.provisioning_pool_start` |  | `"172.20.39.213"` |
| `capm3.provisioning_pool_prefix` |  | `"26"` |
| `capm3.provisioning_pool_interface` |  | `"bond0"` |
| `capm3.dns_server` |  | `"10.193.21.160"` |
| `capm3.machine_image_url` | URL for BM CP & MD node image on a webserver | `"http://55.55.55.55/ubuntu-22.04-plain.qcow2"` |
| `capm3.machine_image_format` | format for BM CP & MD node image | `"qcow2"` |
| `capm3.machine_image_checksum` | checksum for BM CP & MD node image, hosted on a webserver | `"http://55.55.55.55/ubuntu-22.04-plain.qcow2.sha256sum"` |
| `capm3.machine_image_checksum_type` | checksum type for BM CP & MD node image | `"md5"` |
| `capm3.nodeReuse` | boolean conditioning CAPM3 Machine controller to pick the same pool of BMHs' that were released during the upgrade operation, see [Metal3MachineTemplate.spec.nodeReuse](https://github.com/metal3-io/metal3-docs/blob/main/design/cluster-api-provider-metal3/node_reuse.md#node-reuse) | `true` |
| `capm3.automatedCleaningMode` | string, value "disabled" means don’t perform [automated cleaning](https://docs.openstack.org/ironic/latest/admin/cleaning.html#automated-cleaning), while "metadata" enables automated cleaning for BM CP & MD nodes | `"metadata"` |
| `capm3.networkData.provisioning_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `capm3.networkData.public_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `baremetal_host_default.credentials.username` | BMC inteface username | `""` |
| `baremetal_host_default.credentials.password` | BMC inteface password | `""` |
| `baremetal_host_default.bmh_metadata.labels` |  | `{}` |
| `baremetal_host_default.bmh_metadata.annotations` |  | `{}` |
| `baremetal_host_default.bmh_spec` | [BMO BMH spec](https://github.com/metal3-io/baremetal-operator/blob/main/docs/api.md#baremetalhost-spec) | `{}` |
| `baremetal_hosts` | set of BareMetalHosts | `{}` |
| `kubelet_extra_args` | dict of kubelet args | `anonymous-auth: "false"` |
| `rke2.additionalUserData` | mapped to `RKE2ControlPlane.spec.agentConfig.additionalUserData` & `RKE2ConfigTemplate.spec.template.spec.agentConfig.additionalUserData` (with `.config` being a dictionary) | `{}` |
| `rke2.nodeLabels` | dict specifying labels for CP & MD nodes | `{}` |
| `rke2.nodeAnnotations` | dict specifying annotations for CP & MD nodes | `{}` |
{:/comment}
| `kubeadm` | mapped to `KubeadmControlPlane.spec.kubeadmConfigSpec` & `KubeadmConfigTemplate.spec.template.spec` | `{}` |
| `additional_commands.pre_bootstrap_commands` | set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `additional_commands.post_bootstrap_commands` | set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `additional_files` | dict of files to be injected as CAPI Machine file
| `additional_files.x.content` | string to be used as content of the CAPI Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents | `""` |
| `additional_files.x.content_file` | the path of a file with the actual content of the CAPI Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents; only used if `content` is not provided | `""` |
| `additional_files.x.content_k8s_secret` | a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI Machine file; only used if neither `content` nor `content_file` is provided | `{}` |
| `additional_files.x.content_k8s_secret.key` | the key in the secret's data map for this value | `""` |
| `additional_files.x.content_k8s_secret.name` | name of the K8s secret (in the chart release namespace) to use | `""` |
| `additional_files.x.encoding` | specifies the encoding of the provided file contents ("base64", "gzip" or "zip+base64"); skip if plain-text | `""` |
| `additional_files.x.owner` | the ownership of the CAPI Machine file, e.g. "root:root" | `""` |
| `additional_files.x.path` | (optional, defaults additional_files entry x) the full path on disk where to store the CAPI Machine file | `""` |
| `additional_files.x.permissions` | the permissions to assign to the CAPI Machine file, e.g. "0640" | `""` |
| `control_plane.kubelet_extra_args` |  | `anonymous-auth: "false"` |
| `control_plane.rke2.additionalUserData` | mapped to `RKE2ControlPlane.spec.agentConfig.additionalUserData` (with `.config` being a dictionary) | `{}` |
| `control_plane.rke2.nodeLabels` | dict specifying labels for CP nodes | `{}` |
| `control_plane.rke2.nodeAnnotations` | dict specifying annotations for CP nodes | `{}` |
| `control_plane.kubeadm` | mapped to `KubeadmControlPlane.spec.kubeadmConfigSpec` | `{}` |
| `control_plane.additional_commands.pre_bootstrap_commands` | set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `control_plane.additional_commands.post_bootstrap_commands` | set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `control_plane.additional_files` | dict of files to be injected as CAPI CP Machine file
| `control_plane.additional_files.x.content` | string to be used as content of the CAPI CP Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents | `""` |
| `control_plane.additional_files.x.content_file` | the path of a file with the actual content of the CAPI CP Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents; only used if `content` is not provided | `""` |
| `control_plane.additional_files.x.content_k8s_secret` | a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI CP Machine file; only used if neither `content` nor `content_file` is provided | `{}` |
| `control_plane.additional_files.x.content_k8s_secret.key` | the key in the secret's data map for this value | `""` |
| `control_plane.additional_files.x.content_k8s_secret.name` | name of the K8s secret (in the chart release namespace) to use | `""` |
| `control_plane.additional_files.x.encoding` | specifies the encoding of the provided file contents ("base64", "gzip" or "zip+base64"); skip if plain-text | `""` |
| `control_plane.additional_files.x.owner` | the ownership of the CAPI CP Machine file, e.g. "root:root" | `""` |
| `control_plane.additional_files.x.path` | (optional, defaults additional_files entry x) the full path on disk where to store the CAPI CP Machine file | `""` |
| `control_plane.additional_files.x.permissions` | the permissions to assign to the CAPI CP Machine file, e.g. "0640" | `""` |
| `control_plane.capo.flavor_name` |  | `"m1.large"` |
| `control_plane.capo.rootVolume` | (optional) Let this parameter empty if you don't intend to use root volume for CAPO CP nodes | `{}` |
| `control_plane.capo.server_group_id` | generated by heat-operator in sylva | `"replace_me"` |
| `control_plane.capo.security_group_name` | OpenStack SG for control plane nodes | `"capo-cluster-security-group-ctrl-plane"` |
| `control_plane.capv.diskGiB` | disk size for CP vSphereMachineTemplate | `25` |
| `control_plane.capv.memoryMiB` | memory size for CP vSphereMachineTemplate | `8192` |
| `control_plane.capv.numCPUs` | CPUs for CP vSphereMachineTemplate | `2` |
| `control_plane.capm3.hostSelector.matchLabels.cluster-role` |  | `"control-plane"` |
| `control_plane.capm3.nodeReuse` | boolean conditioning CAPM3 Machine controller to pick the same pool of CP BMHs' that were released during the upgrade operation, see [Metal3MachineTemplate.spec.nodeReuse](https://github.com/metal3-io/metal3-docs/blob/main/design/cluster-api-provider-metal3/node_reuse.md#node-reuse) | `true` |
| `control_plane.capm3.automatedCleaningMode` | string, value "disabled" means don’t perform automated cleaning, while "metadata" enables automated cleaning for BM CP nodes | `"metadata"` |
| `control_plane.capm3.networkData.provisioning_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `control_plane.capm3.networkData.public_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `control_plane.capm3.provisioning_pool_interface` |  | `"bond0"` |
| `control_plane.capm3.public_pool_interface` |  | `""` |
| `control_plane.network_interfaces` |  | `{}` |
| `machine_deployment_default.replicas` | default number of worker nodes for each MachineDeployment | `0` |
| `machine_deployment_default.metadata.labels` |  | `{}` |
| `machine_deployment_default.metadata.annotations` |  | `{}` |
| `machine_deployment_default.kubelet_extra_args` |  | `anonymous-auth: "false"` |
| `machine_deployment_default.rke2.additionalUserData` | mapped to `RKE2ConfigTemplate.spec.template.spec.agentConfig.additionalUserData` (with `.config` being a dictionary) | `{}` |
| `machine_deployment_default.rke2.nodeLabels` | dict specifying labels for MD nodes | `{}` |
| `machine_deployment_default.rke2.nodeAnnotations` | dict specifying annotations for MD nodes | `{}` |
| `machine_deployment_default.kubeadm` | mapped to `KubeadmConfigTemplate.spec.template.spec` | `{}` |
| `machine_deployment_default.additional_commands.pre_bootstrap_commands` | set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `machine_deployment_default.additional_commands.post_bootstrap_commands` | set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `machine_deployment_default.additional_files` | dict of files to be injected as CAPI MD Machine file
| `machine_deployment_default.additional_files.x.content` | string to be used as content of the CAPI MD Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents | `""` |
| `machine_deployment_default.additional_files.x.content_file` | the path of a file with the actual content of the CAPI MD Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents; only used if `content` is not provided | `""` |
| `machine_deployment_default.additional_files.x.content_k8s_secret` | a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI MD Machine file; only used if neither `content` nor `content_file` is provided | `{}` |
| `machine_deployment_default.additional_files.x.content_k8s_secret.key` | the key in the secret's data map for this value | `""` |
| `machine_deployment_default.additional_files.x.content_k8s_secret.name` | name of the K8s secret (in the chart release namespace) to use | `""` |
| `machine_deployment_default.additional_files.x.encoding` | specifies the encoding of the provided file contents ("base64", "gzip" or "zip+base64"); skip if plain-text | `""` |
| `machine_deployment_default.additional_files.x.owner` | the ownership of the CAPI MD Machine file, e.g. "root:root" | `""` |
| `machine_deployment_default.additional_files.x.path` | (optional, defaults additional_files entry x) the full path on disk where to store the CAPI MD Machine file | `""` |
| `machine_deployment_default.additional_files.x.permissions` | the permissions to assign to the CAPI MD Machine file, e.g. "0640" | `""` |
| `machine_deployment_default.capo.image_name` |  | `""` |
| `machine_deployment_default.capo.flavor_name` |  | `"m1.large"` |
| `machine_deployment_default.capo.server_group_id` | generated by heat-operator in sylva | `"replace_me"` |
| `machine_deployment_default.capo.security_group_name` | OpenStack SG for worker nodes | `"capo-cluster-security-group-worker"` |
| `machine_deployment_default.capo.rootVolume` | (optional) Let this parameter empty if you don't intend to use root volume for CAPO MD nodes | `{}` |
| `machine_deployment_default.capo.network_id` | (optional) OpenStack VN to attach MD nodes to, defaults to `.capo.network_id` | `"replace_me"` |
| `machine_deployment_default.capo.failure_domain` | (optional) OpenStack AZ for CAPO MD nodes | `"replace_me"` |
| `machine_deployment_default.capo.identity_ref_secret.clouds_yaml` | (optional) OpenStack clouds.yaml content for CAPO MD nodes, defaults to `.capo.clouds_yaml` if not provided | `"replace_me"` |
| `machine_deployment_default.capo.identity_ref_secret.clouds_yaml` | (optional) OpenStack clouds.yaml Secret name for CAPO MD nodes | `"replace_me"` |
| `machine_deployment_default.capv.image_name` |  | `""` |
| `machine_deployment_default.capv.diskGiB` | disk size for MD vSphereMachineTemplate | `25` |
| `machine_deployment_default.capv.memoryMiB` | memory size for MD vSphereMachineTemplate | `8192` |
| `machine_deployment_default.capv.numCPUs` | CPUs for MD vSphereMachineTemplate | `2` |
| `machine_deployment_default.capm3.hostSelector.matchLabels.cluster-role` |  | `"worker"` |
| `machine_deployment_default.capm3.nodeReuse` | boolean conditioning CAPM3 Machine controller to pick the same pool of MD BMHs' that were released during the upgrade operation, see [Metal3MachineTemplate.spec.nodeReuse](https://github.com/metal3-io/metal3-docs/blob/main/design/cluster-api-provider-metal3/node_reuse.md#node-reuse) | `true` |
| `machine_deployment_default.capm3.automatedCleaningMode` | string, value "disabled" means don’t perform automated cleaning, while "metadata" enables automated cleaning for BM MD nodes | `"metadata"` |
| `machine_deployment_default.capm3.networkData.provisioning_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `machine_deployment_default.capm3.networkData.public_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `machine_deployment_default.capm3.provisioning_pool_interface` |  | `"bond0"` |
| `machine_deployment_default.capm3.public_pool_interface` |  | `""` |
| `machine_deployment_default.network_interfaces` | set of default values for MachineDeployments network interfaces | `{}` |
| `machine_deployment_default.machine_deployment_spec` | set of default fields for MachineDeployment.spec | `{}` |
| `machine_deployments` | set of MachineDeployments | `{}` |
| `machine_deployments.x.machine_deployment_spec` | set of fields for MachineDeployment.X.spec  | `{}` |
| `images.kube_vip.repository` |  | `"ghcr.io/kube-vip/kube-vip"` |
| `images.kube_vip.tag` |  | `"v0.5.12"` |
| `ntp` | Let this parameter empty if you don't intend to configure NTP on machines | `{}` |
| `proxies.https_proxy` |  | `""` |
| `proxies.http_proxy` |  | `""` |
| `proxies.no_proxy` |  | `""` |
| `registry_mirrors` | add your local registry mirror to avoid rate limiting | `{}` |
| `bgp_lbs.kube_vip` | add your kube-vip config if required | `{}` |
| `bgp_lbs.metallb` | add your metallb-l3 config if required | `{}` |

---
_Documentation generated by [Frigate](https://frigate.readthedocs.io)._

## Developing in this chart

As a developer in Sylva, or as a developer in a team using `sylva-capi-cluster` and wanting to add new functionality additionally to existing ones, you need to consider that this chart tries to handle properly the lifecycle of [Infrastructure Machine Templates](https://cluster-api.sigs.k8s.io/tasks/updating-machine-templates.html#updating-infrastructure-machine-templates) and [Bootstrap Templates](https://cluster-api.sigs.k8s.io/tasks/updating-machine-templates.html#updating-bootstrap-templates) (or [metaData and networkData](https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/docs/api.md#updating-metadata-and-networkdata) in the particular case of [`Metal3`](https://github.com/metal3-io/cluster-api-provider-metal3)), by creating a new resource (achieved by appending a 10 digit subset from the hash of its immutable spec content) in order to ensure that a new object will be created whenever one of its (immutable) parameters is changed. <br/>
For this reason, the chart structure does come with a certain level of complexity, all the CAPI resources having specs with immutable fields being available as named templates (to handle the lifecycle of resource). The following diagrams are meant to assist in understanding this structure. <br/>

![sylva-capi-cluster chart cp](docs/img/sylva-capi-cluster-cp.drawio.png)
![sylva-capi-cluster chart md](docs/img/sylva-capi-cluster-md.drawio.png)

### The values override structure

When writing this chart we had in mind the ability to override at multiple levels and thus we ended up with the following types of values:

- Global parameters that apply at cluster level.

Examples of these are `.name`, `.k8s_version`, `.control_plane_replicas`, all the infrastructure providers (`.capd`, `.capo`, `.capv`, `.capm3`), `.baremetal_hosts` and so on.

- Global parameters that can be overriden for a MachineDeployment (under `.machine_deployment_default` or `.machine_deployments.X`) mainly for usecases as specifying an infrastructure provider for an MD which could be different than CP nodes' one (for background, please see [sylva-core/-/issues/322](https://gitlab.com/sylva-projects/sylva-core/-/issues/322)) or installing worker nodes over multiple OpenStack platforms (for background, please see [sylva-core/-/issues/276](https://gitlab.com/sylva-projects/sylva-core/-/issues/276)) or modifying any fields of  MachineDeployments on machine_deployment_spec (for background, please see [https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/issues/79](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/issues/79)). Such examples would be: <br/>

> - `.capi_providers.infra_provider` by `.machine_deployments.X.infra_provider`, <br/>
> - `.capo.clouds_yaml` by `.machine_deployments.X.capo.identity_ref_secret.clouds_yaml` <br/>
> - `.capo.network_id` by `.machine_deployments.X.capo.network_id`. <br/>
> - or `.strategy.rollingUpdate.maxUnavailable` by `.machine_deployments.X.machine_deployment_spec.strategy.rollingUpdate.maxUnavailable`.

- Global parameters that apply to all machines (CP and MD) and which can get overriden for each role.

Such values are `.kubelet_extra_args`, `.rke2`, `.kubeadm` and some specific keys of cluster level parameters (like `.capv.memoryMiB` for example) and can be overriden for the CP specific needs by their equivalent under `.control_plane` and for MD under  `.machine_deployment_default` or `.machine_deployments.X`, respectively. For these values following _machine_deployments > machine_deployment_default > default_ and _control_plane > default_ logic, we'd have for example:

```yaml

capo:
  flavor_name: m1.medium # default OpenStack flavor name for both CP & MD

control_plane:
  capo:
    flavor_name: m1.large # OpenStack flavor name for CP only

machine_deployment_default:
  capo:
    flavor_name: m2.small # OpenStack flavor name for all MDs

machine_deployments:
  md0:
    capo:
      flavor_name: m2.medium # OpenStack flavor name for specifc MD only

```

and the templates would compute the most meaningful one (i.e. the values in `.control_plane.capo.flavor_name` and `.machine_deployments.md0.capo.flavor_name` respectively, for the above example).

<details markdown=1><summary>
Code sample (click to expand)
</summary>

We make fair use of [pluck](https://helm.sh/docs/chart_template_guide/function_list/#pluck) & [mergeoverwrite](https://helm.sh/docs/chart_template_guide/function_list/#mergeoverwrite-mustmergeoverwrite) to implement the override logic.

```go

// templates/_openstackmachinetemplatespec-cp.tpl
flavor: {{ pluck "flavor_name" .Values.capo (.Values.control_plane.capo | default dict) | last }}

```

```go

// templates/_openstackmachinetemplatespec-md.tpl
flavor: {{ pluck "flavor_name" $envAll.Values.capo ($machine_capo_specs | default dict) | last }}

```

where `$machine_capo_specs` is `$machine_deployment_def.capo` and `$machine_deployment_def` is already a merge of `.machine_deployments.X` with `.machine_deployment_default`, per

```go

{{- range $machine_deployment_name, $machine_deployment_specs := $envAll.Values.machine_deployments }}

  {{/*********** Finalize the definition of the machine_deployments.X
  item by merging the machine_deployment_default */}}
  {{- $machine_deployment_def := dict -}}
  {{- $machine_deployment_def := deepCopy ($envAll.Values.machine_deployment_default | default dict) -}}
  {{- $machine_deployment_def := mergeOverwrite $machine_deployment_def $machine_deployment_specs -}}

```

block we have in

```shell

templates/workers.yaml
templates/rke2configtemplate.yaml
templates/kubeadmconfigtemplate.yaml
templates/_Xmachinetemplate-md.tpl

```

</details>

> NOTE: some notable mention for this kind of values is the image to be used on the cluster nodes, for which depending on the infrastructure provider we are dealing with different concepts (an image reference for CAPO & CAPV, a container image for CAPD and an URL for CAPM3). Such an `machine_deployment_default.<infra_provider>`/`.machine_deployments.X.<infra_provider>`  OS image override comes useful when a specific MD needs to use a different image and/or is on a different infrastructure (has `.machine_deployments.X.infra_provider` different than `.capi_providers.infra_provider`). These options are:

```yaml

# Docker/OpenStack/vSphere/Metal3 image definition for both CP & MD
capd:
  image_name: registry.gitlab.com/sylva-projects/sylva-elements/container-images/rke2-in-docker:v1-24-12-rke2r1
capo:
  image_name: "Ubuntu 20.04"
capv:
  image_name: "Ubuntu 20.04 Packer"
capm3:
  machine_image_url: http://55.55.55.55/ubuntu-22.04-plain.qcow2 # URL for BM CP & MD node image on a webserver
  machine_image_format: qcow2 # format for BM CP & MD node image
  machine_image_checksum: http://55.55.55.55/ubuntu-22.04-plain.qcow2.sha256sum # checksum for BM CP & MD node image, hosted on a webserver
  machine_image_checksum_type: md5 # checksum type for BM CP & MD node image

control_plane: # Docker/OpenStack/vSphere image name for CP only
  capd:
    image_name: "replace_me"
  capo:
    image_name: "replace_me"
  capv:
    image_name: "replace_me"
  capm3: # Metal3 image definition for CP only
    machine_image_url: "replace_me" # URL for BM CP image on a webserver
    machine_image_format: qcow2 # format for BM CP node image
    machine_image_checksum: "replace_me" # checksum for BM CP node image, hosted on a webserver
    machine_image_checksum_type: md5 # checksum type for BM CP node image

machine_deployment_default: # OpenStack/vSphere image name for all MDs
  capo:
    image_name: "Ubuntu 22.04 Packer"
  capv:
    image_name: "Ubuntu 22.04 Packer"
  capm3: # Metal3 image definition for all MDs
    machine_image_url: "replace_me" # URL for BM MD node image on a webserver
    machine_image_format: qcow2 # format for BM MD node image
    machine_image_checksum: "replace_me" # checksum for BM MD node image, hosted on a webserver
    machine_image_checksum_type: md5 # checksum type for BM MD node image

machine_deployments: # OpenStack/vSphere image name for specifc MD only (no MD for CAPD)
  md0:
    infra_provider: capo
    capo:
      image_name: "Ubuntu 20.04 Packer CIS"
  md1:
    infra_provider: capv
    capv:
      image_name: "Ubuntu 22.04 Packer CIS"

```
