{{- if eq .Values.capi_providers.bootstrap_provider "cabpk" }}
{{ include "XMachineTemplate-CP" . }}

  {{/*********** Initialize the components of the KubeadmControlPlane.spec.kubeadmConfigSpec fields */}}
  {{- $base := include "base-kubeadmConfigSpec-CP" . | fromYaml }}
  {{- $infra := include (printf "%s-kubeadmConfigSpec-CP" .Values.capi_providers.infra_provider) . | fromYaml }}

  {{/*********** Set the default control_plane.kubeadm to empty dict, if not specifically defined */}}
  {{ if not (.Values.control_plane.kubeadm) }}
  {{- $_ := set .Values.control_plane "kubeadm" (dict) -}}
  {{ end }}

  {{/*********** Set the default .kubeadm to empty dict, if not specifically defined */}}
  {{ if not (.Values.kubeadm) }}
  {{- $_ := set .Values "kubeadm" (dict) -}}
  {{ end }}

  {{/*********** Append user-provided API Cert SANs over infra ones */}}
  {{- $APICertSANs := concat (dig "clusterConfiguration" "apiServer" "certSANs" list $infra) (.Values.cluster_api_cert_extra_SANs | default list) }}
---
apiVersion: {{ .Values.apiVersions.KubeadmControlPlane }}
kind: KubeadmControlPlane
metadata:
  name: {{ .Values.name }}-control-plane
  namespace: {{ .Release.Namespace }}
spec:
  replicas: {{ ternary "1" .Values.control_plane_replicas (eq .Values.capi_providers.infra_provider "capd") }}
  version: {{ .Values.k8s_version }}
  machineTemplate:
{{ if or (eq .Values.capi_providers.infra_provider "capo") (eq .Values.capi_providers.infra_provider "capv") }}
    metadata:
      labels:
        health-check: control-plane
{{ end }}
    infrastructureRef:
{{ if eq .Values.capi_providers.infra_provider "capd" }}
      apiVersion: {{ .Values.apiVersions.DockerMachineTemplate }}
      kind: DockerMachineTemplate
      name: {{ .Values.name }}-cp-{{ include "DockerMachineTemplateSpec" . | sha1sum | trunc 10 }}
{{ else if eq .Values.capi_providers.infra_provider "capo" }}
      apiVersion: {{ .Values.apiVersions.OpenStackMachineTemplate }}
      kind: OpenStackMachineTemplate
      name: {{ .Values.name }}-cp-{{ include "OpenStackMachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
{{ else if eq .Values.capi_providers.infra_provider "capv" }}
      apiVersion: {{ .Values.apiVersions.VSphereMachineTemplate }}
      kind: VSphereMachineTemplate
      name: {{ .Values.name }}-cp-{{ include "VSphereMachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
{{ else if eq .Values.capi_providers.infra_provider "capm3" }}
      apiVersion: {{ .Values.apiVersions.Metal3MachineTemplate }}
      kind: Metal3MachineTemplate
      name: {{ .Values.name }}-cp-{{ include "Metal3MachineTemplateSpec-CP" . | include "Metal3MachineTemplateSpec-remove-url-hostname" | sha1sum | trunc 10 }}
{{ end }}
  kubeadmConfigSpec:
    initConfiguration: {{ mergeOverwrite $base.initConfiguration $infra.initConfiguration | toYaml | nindent 6 }}
    joinConfiguration: {{ mergeOverwrite $base.joinConfiguration $infra.joinConfiguration | toYaml | nindent 6 }}
    clusterConfiguration: {{ mergeOverwrite $base.clusterConfiguration $infra.clusterConfiguration (dict "apiServer" (dict "certSANs" $APICertSANs )) | toYaml | nindent 6 }}
    {{- if and .Values.ntp (not (eq .Values.capi_providers.infra_provider "capd")) }} {{/* CAPD infra provider does not support NTP configuration for KubeadmControlPlane.spec.kubeadmConfigSpec */}}
    ntp: {{ mergeOverwrite $base.ntp $infra.ntp | toYaml | nindent 6 }}
    {{- end }}
    # TODO: check if all infra providers can agree on same set of KubeadmControlPlane.spec.kubeadmConfigSpec.preKubeadmCommands
    # except the .Values.dns_resolver (DNS client config) scenario, which differs for CAPD (no service restart)
    {{- $cp_additional_commands := deepCopy (.Values.additional_commands | default dict) -}}
    {{- if .Values.control_plane.additional_commands }}
      {{- tuple $cp_additional_commands .Values.control_plane.additional_commands | include "merge-append" }}
    {{- end }}
    preKubeadmCommands:
      {{ $infra.preKubeadmCommands | toYaml | nindent 6 }}
      {{ $base.preKubeadmCommands | toYaml | nindent 6 }}
    {{- if $cp_additional_commands.pre_bootstrap_commands }}
      {{- $cp_additional_commands.pre_bootstrap_commands | toYaml | nindent 6 }}
    {{- end }}
    files: {{ concat $base.files $infra.files | default list | toYaml | nindent 6 }}
    postKubeadmCommands:
      {{ $infra.postKubeadmCommands | toYaml | nindent 6 }}
      {{ $base.postKubeadmCommands | toYaml | nindent 6 }}
    {{- if $cp_additional_commands.post_bootstrap_commands }}
      {{- $cp_additional_commands.post_bootstrap_commands | toYaml | nindent 6 }}
    {{- end }}
    users: {{ mergeOverwrite (deepCopy .Values.kubeadm) (deepCopy .Values.control_plane.kubeadm) | dig "users" list | toYaml | nindent 6 }}
{{- end }}
