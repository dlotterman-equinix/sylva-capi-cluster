{{- define "capd-RKE2ControlPlaneSpec" }}
registrationMethod: internal-first
agentConfig:
  # TODO: test a rke2-capd deployment with "cis_profile: cis-1.23" to see what breaks
  # excluding from rke2-capd for now
  cisProfile: null
  kubelet:
    extraArgs:
      - "eviction-hard=nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%"
  ntp: null {{/* CAPD infra provider does not support NTP configuration for RKE2ControlPlane.spec.agentConfig */}}
serverConfig:
  tlsSan:
    - localhost
    - 127.0.0.1
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capo-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "provider-id=openstack:///{{`{{ ds.meta_data.uuid }}`}}"
  {{- if .Values.enable_longhorn }}
  nodeLabels:
    - node.longhorn.io/create-default-disk=true
  {{- end }}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  {{- if .Values.dns_resolver }}
  - | {{ tuple "false" .Values.mgmt_cluster_external_ip | include "shell-opensuse-dns" | nindent 4 }}
  {{- else }}
  - | {{ tuple "true" "_unused_" | include "shell-opensuse-dns" | nindent 4 }}
  {{- end }}
files: []
{{- end }}

{{- define "capv-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "--cloud-provider=external"
serverConfig:
  disableComponents:
    kubernetesComponents:
      - "cloudController"
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capm3-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      - "provider-id=metal3://{{`{{ ds.meta_data.providerid }}`}}"
  nodeName: {{`'{{ ds.meta_data.local_hostname }}'`}}
  {{- if .Values.enable_longhorn }}
  nodeLabels:
    - node.longhorn.io/create-default-disk={{`{{ds.meta_data.longhorn}}`}}
  {{- end }}
preRKE2Commands:
  - netplan apply
  - sleep 30 # fix to give OS time to become ready
  {{- if .Values.dns_resolver }}
  - | {{ tuple "false" .Values.mgmt_cluster_external_ip | include "shell-opensuse-dns" | nindent 4 }}
  {{- else }}
  - | {{ tuple "false" .Values.capm3.dns_server | include "shell-opensuse-dns" | nindent 4 }}
  {{- end }}
  {{- if .Values.enable_longhorn }}
  - | {{ include "shell-longhorn-mounts" . | nindent 4 }}
  {{- end }}
files: []
{{- end }}
