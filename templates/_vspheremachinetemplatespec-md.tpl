{{ define "VSphereMachineTemplateSpec-MD" }}
{{- $envAll := index . 0 -}}
{{- $machine_network_interfaces := index . 1 -}}
{{- $machine_capv_specs := index . 2 -}}
cloneMode: {{ $envAll.Values.capv.template_clone_mode }}
datacenter: {{ $envAll.Values.capv.dataCenter | quote }}
{{- if $envAll.Values.capv.dataStore }}
datastore: {{ $envAll.Values.capv.dataStore }}
{{- end }}
folder: {{ $envAll.Values.capv.folder }}
resourcePool: {{ $envAll.Values.capv.resourcePool }}
server: {{ $envAll.Values.capv.server }}
{{- if $envAll.Values.capv.storagePolicyName }}
storagePolicyName: {{ $envAll.Values.capv.storagePolicyName }}
{{- end }}
template: {{ pluck "image_name" $envAll.Values.capv ($machine_capv_specs | default dict) | last | required "'image_name' needs to be defined under capv or machine_deployment(_default|s.xxx).capv" }}
thumbprint: {{ $envAll.Values.capv.tlsThumbprint }}
diskGiB: {{ pluck "diskGiB" $envAll.Values.capv ($machine_capv_specs | default dict) | last | required "'diskGiB' needs to be defined under capv or machine_deployment(_default|s.xxx).capv" }}
memoryMiB: {{ pluck "memoryMiB" $envAll.Values.capv ($machine_capv_specs | default dict) | last | required "'memoryMiB' needs to be defined under capv or machine_deployment(_default|s.xxx).capv" }}
numCPUs: {{ pluck "numCPUs" $envAll.Values.capv ($machine_capv_specs | default dict) | last | required "'numCPUs' needs to be defined under capv or machine_deployment(_default|s.xxx).capv" }}
network:
  devices:
  {{- range $common_network_interface_name, $common_network_interface_def := $envAll.Values.capv.networks }}
  {{- if $common_network_interface_def.networkName }}
    - dhcp4: {{ $common_network_interface_def.dhcp4 | default false }}
      networkName: {{ $common_network_interface_def.networkName }}
  {{- end }}
  {{- end }}
  {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
  {{- if $machine_network_interface_def.networkName }}
    - dhcp4: {{ $machine_network_interface_def.dhcp4 | default false }}
      networkName: {{ $machine_network_interface_def.networkName }}
  {{- end }}
  {{- end }}
{{ end }}
