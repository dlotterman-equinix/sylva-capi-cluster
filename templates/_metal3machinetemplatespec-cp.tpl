{{ define "Metal3MachineTemplateSpec-CP" }}
nodeReuse: {{ pluck "nodeReuse" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last }}
template:
  spec:
    automatedCleaningMode: {{ pluck "automatedCleaningMode" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last }}
    dataTemplate:
      name: {{ .Values.name }}-cp-metadata-{{ include "Metal3DataTemplateSpec-CP" . | sha1sum | trunc 10 }}
    hostSelector: {{ .Values.control_plane.capm3.hostSelector | required "'hostSelector' needs to be defined under capm3 or control_plane.capm3" | toYaml | nindent 6 }}
    image:
      checksum: {{ pluck "machine_image_checksum" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last | required "'machine_image_checksum' needs to be defined under capm3 or control_plane.capm3" }}
      checksumType: {{ pluck "machine_image_checksum_type" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last | required "'machine_image_checksum_type' needs to be defined under capm3 or control_plane.capm3" }}
      format: {{ pluck "machine_image_format" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last | required "'machine_image_format' needs to be defined under capm3 or control_plane.capm3" }}
      url: {{ pluck "machine_image_url" .Values.capm3 (.Values.control_plane.capm3 | default dict) | last | required "'machine_image_url' needs to be defined under capm3 or control_plane.capm3" }}
{{ end }}

{{ define "Metal3DataTemplate-CP" }}
apiVersion: {{ .Values.apiVersions.Metal3DataTemplate }}
kind: Metal3DataTemplate
metadata:
  name: {{ .Values.name }}-cp-metadata-{{ include "Metal3DataTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
spec:
# NOTE: the Metal3DataTemplate.spec fields .metaData & .networkData are immutable
# per https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/docs/api.md#updating-metadata-and-networkdata
{{ include "Metal3DataTemplateSpec-CP" . | indent 2 }}
{{ end }}

{{ define "Metal3DataTemplateSpec-CP" }}
# https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/368
# TODO: try to templatize, get diff with https://github.com/metal3-io/cluster-api-provider-metal3/blob/6a9fe1d9e9fbea791094a709d5e79ad99c63b34b/examples/machinedeployment/machinedeployment.yaml#LL56C1-L136C18
clusterName: {{ .Values.name }}
metaData:
  ipAddressesFromIPPool:
  - kind: IPPool
    apiGroup: ipam.metal3.io
    name: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
    key: provisioningIP
  objectNames:
  - key: name
    object: machine
  - key: local-hostname
    object: machine
  - key: local_hostname
    object: machine
  prefixesFromIPPool:
  - kind: IPPool
    apiGroup: ipam.metal3.io
    name: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
    key: provisioningCIDR
  {{- if .Values.enable_longhorn }}
  fromLabels:
  - key: longhorn
    object: baremetalhost
    label: longhorn
  fromAnnotations:
  - key: disk
    object: baremetalhost
    annotation: disk
  - key: sylva_longhorn_disks
    object: baremetalhost
    annotation: sylvaproject.org/longhorn-disks
  {{- end }}
networkData:
  links:
    ethernets:
    {{- range $interface_name, $interface_def := .Values.control_plane.network_interfaces }}
    {{- if not (hasKey $interface_def "bond_mode") }}
    - id: {{ $interface_name }}
      macAddress:
        fromHostInterface: {{ dig "macAddress" "fromHostInterface" $interface_name $interface_def }}
      type: {{ dig "type" "phy" $interface_def  }}
    {{- end }}
    {{- end }}
    bonds:
    {{- range $interface_name, $interface_def := .Values.control_plane.network_interfaces }}
    {{- if hasKey $interface_def "bond_mode" }}
    - id: {{ $interface_name }}
      bondLinks:
        {{- range $bond_interfaces := $interface_def.interfaces }}
        - {{ $bond_interfaces }}
        {{- end }}
      bondMode: {{ $interface_def.bond_mode }}
      macAddress: {}
    {{- end }}
    {{- end }}
    vlans:
    {{- range $interface_name, $interface_def := .Values.control_plane.network_interfaces }}
    {{- if hasKey $interface_def "bond_mode" }}
{{ tuple $ $interface_name $interface_def | include "listOfVlansPerBond" | indent 4 }}
    {{- else }}
{{ tuple $ $interface_name $interface_def | include "listOfVlansPerInterface" | indent 4 }}
    {{- end }}
    {{- end }}
  networks:
    ipv4:
    - id: {{ .Values.control_plane.capm3.provisioning_pool_interface | default .Values.capm3.provisioning_pool_interface | required "provisioning_pool_interface needs to be specified under capm3 or control_plane.capm3" }}
      ipAddressFromIPPool: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
      link: {{ .Values.control_plane.capm3.provisioning_pool_interface | default .Values.capm3.provisioning_pool_interface | required "provisioning_pool_interface needs to be specified under capm3 or control_plane.capm3" }}
      routes: {{ pluck "routes" (.Values.capm3.networkData.provisioning_pool_interface | default dict) (.Values.control_plane.capm3.networkData.provisioning_pool_interface | default dict) | last | default list | toYaml | nindent 8 }}
    - id: {{ .Values.control_plane.capm3.public_pool_interface | default .Values.capm3.public_pool_interface | required "public_pool_interface needs to be specified under capm3 or control_plane.capm3" }}
      ipAddressFromIPPool: {{ printf "%s-%s" .Values.name .Values.capm3.public_pool_name }}
      link: {{ .Values.control_plane.capm3.public_pool_interface | default .Values.capm3.public_pool_interface | required "public_pool_interface needs to be specified under capm3 or control_plane.capm3" }}
      {{- if or .Values.capm3.networkData.public_pool_interface .Values.control_plane.capm3.networkData.public_pool_interface }}
      routes:
{{ pluck "routes" .Values.capm3.networkData.public_pool_interface .Values.control_plane.capm3.networkData.public_pool_interface | last | toYaml | indent 8 }}
      {{- else }}
      routes:
        - gateway:
            fromIPPool: {{ printf "%s-%s" .Values.name .Values.capm3.public_pool_name }}
          network: 0.0.0.0
      {{- end }}
  services:
    dns:
    - {{ .Values.capm3.dns_server }}
{{ end }}
