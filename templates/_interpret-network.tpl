{{/*
Return a list of all VLANs that are configured on same bond

values: |
  tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerBond"

usage: |
  In values.yaml:
    machine_deployments:
      md0:
        :
        network_interfaces:
          bond0:
            bond_mode: 802.3ad
            interfaces:
              - ens1fO
              - ens1f1
            vlans:
              - id: 92
          bond1:
            bond_mode: 802.3ad
            interfaces:
              - ens2fO
              - ens2f1
            vlans:
              - id: 206

  Inside Metal3DataTemplateSpec-MD named template:
    vlans:
    {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
    {{- if hasKey $machine_network_interface_def "bond_mode" }}
{{ tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerBond" | indent 4}}
    {{- else }}
{{ tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerInterface" | indent 4}}
    {{- end }}
    {{- end }}

return: |
  networkData:
    links:
      vlans:
      - id: bond0.92
        vlanID: 92
        vlanLink: bond0
        macAddress:
          fromHostInterface: ens1f0
      - id: bond1.206
        vlanID: 206
        vlanLink: bond1
        macAddress:
          fromHostInterface: ens2f0
*/}}


{{- define "listOfVlansPerBond" -}}
{{- $envAll := index . 0 -}}
{{- $machine_network_interface_name := index . 1 -}}
{{- $machine_network_interface_def  := index . 2 -}}
{{- $machine_bond_vlan_list := list -}}
{{- if hasKey $machine_network_interface_def "vlans" }}
  {{- range $vlan := $machine_network_interface_def.vlans }}
     {{- $vland_id := cat $machine_network_interface_name "." $vlan.id | nospace | toYaml }}
     {{- $machine_bond_vlan_dict := dict "id" $vland_id  -}}
     {{- $_ := set $machine_bond_vlan_dict "vlanID" $vlan.id -}}
     {{- $_ := set $machine_bond_vlan_dict "vlanLink" $machine_network_interface_name -}}
     {{- $_ := set $machine_bond_vlan_dict  "macAddress" (dict "fromHostInterface" (first $machine_network_interface_def.interfaces | toYaml)) -}}
     {{- $machine_bond_vlan_list = append $machine_bond_vlan_list $machine_bond_vlan_dict -}}
  {{- end -}}
{{- end -}}
{{- if not (empty $machine_bond_vlan_list) -}}
{{- $machine_bond_vlan_list | toYaml }}
{{- end -}}
{{- end -}}

{{/*
Return a list of all VLANs that are configured on same interface

values: |
  tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerInterface"

usage: |
  In values.yaml:
    machine_deployments:
      md0:
        :
        network_interfaces:
          ens1f0:
            vlans:
              - id: 92
          ens1f1:
            vlans:
              - id: 92

  Inside Metal3DataTemplateSpec-MD named template:
    vlans:
    {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
    {{- if hasKey $machine_network_interface_def "bond_mode" }}
{{ tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerBond" | indent 4}}
    {{- else }}
{{ tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerInterface" | indent 4}}
    {{- end }}
    {{- end }}

return: |
  networkData:
    links:
      vlans:
      - id: ens1f0.92
        vlanID: 92
        vlanLink: ens1f0
        macAddress:
          fromHostInterface: ens1f0
      - id: ens1f1.92
        vlanID: 92
        vlanLink: ens1f1
        macAddress:
          fromHostInterface: ens1f1
*/}}

{{- define "listOfVlansPerInterface" -}}
{{- $envAll := index . 0 -}}
{{- $machine_network_interface_name := index . 1 -}}
{{- $machine_network_interface_def  := index . 2 -}}
{{- $machine_interface_vlan_list := list -}}
{{- if hasKey $machine_network_interface_def "vlans" }}
  {{- range $vlan := $machine_network_interface_def.vlans }}
     {{- $vland_id := cat $machine_network_interface_name "." $vlan.id | nospace | toYaml }}
     {{- $machine_interface_vlan_dict := dict "id" $vland_id  -}}
     {{- $_ := set $machine_interface_vlan_dict "vlanID" $vlan.id -}}
     {{- $_ := set $machine_interface_vlan_dict "vlanLink" $machine_network_interface_name -}}
     {{- $_ := set $machine_interface_vlan_dict  "macAddress" (dict "fromHostInterface" $machine_network_interface_name) -}}
     {{- $machine_interface_vlan_list = append $machine_interface_vlan_list $machine_interface_vlan_dict -}}
  {{- end -}}
{{- end -}}
{{- if not (empty $machine_interface_vlan_list) -}}
{{- $machine_interface_vlan_list | toYaml }}
{{- end -}}
{{- end -}}
